package daoPackage;


import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServlet;



import getSetPackage.GetSet;
import utilPackage.Util;

public class Dao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public int insertProfessor(GetSet gs) {
		
		int count = 0;
		try {			
			Connection connection = Util.getDBConnection();
			String query = "insert into professor_details values(?,?,?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, gs.getProfessorId());
			preparedStatement.setString(2, gs.getProfessorName());
			preparedStatement.setString(3, gs.getProfessorEmail());
			preparedStatement.setString(4, gs.getProfessorPassword());
			count = preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}
	public int insertStudent(GetSet gs) {
		int count = 0;
		try {
		Connection connection = Util.getDBConnection();
		String query = "insert into student_details values(?,?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1, gs.getStudentId());
		preparedStatement.setString(2, gs.getStudentName());
		preparedStatement.setString(3, gs.getStudentEmail());
		preparedStatement.setString(4, gs.getStudentPassword());
		count = preparedStatement.executeUpdate();
		} catch (Exception e) {
		System.out.println(e);
		}
		return count;
		}
	public String viewProfessor(GetSet gs) {
		// TODO Auto-generated method stub
		String sts = "false";
		GetSet gs1=new GetSet();
		try {Connection connection = Util.getDBConnection();
			String query = "select ppass from professor_details where pid = ?";
			
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, gs.getProfessorId());
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
				gs1.setProfessorPassword(resultSet.getString("ppass"));
			
					if(gs.getProfessorPassword().equals(gs1.getProfessorPassword())) {
						sts="true";
						
					}
			}
			resultSet.close();
		} catch (Exception e) {
		System.out.println(e);
		}
		return sts;
	}
	
	
	public String viewStudent(GetSet gs) {
		// TODO Auto-generated method stub
		String stss = "false";
		GetSet gs2=new GetSet();
		try {Connection connection = Util.getDBConnection();
			String query = "select spass,sname from student_details where sid = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, gs.getStudentId());
			ResultSet resultSet = preparedStatement.executeQuery();
			if(resultSet.next()) {
					gs2.setStudentPassword(resultSet.getString("spass"));
					gs2.setStudentName(resultSet.getString("sname"));
			
					if(gs.getStudentPassword().equals(gs2.getStudentPassword())) {
						stss=resultSet.getString("sname");
						
					}
			}
			resultSet.close();
		} catch (Exception e) {
		System.out.println(e);
		}
		return stss;
	}
	public boolean addtopic(String topicname) {
		// TODO Auto-generated method stub
		boolean check=false;
		int count = 0;
		try {
		Connection connection = Util.getDBConnection();
		String query = "insert into topics values(?,?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(query);
		preparedStatement.setInt(1,1);
		preparedStatement.setString(2,topicname);
		count = preparedStatement.executeUpdate();
		} catch (Exception e) {
		System.out.println(e);
		}
		if(count==1)
			check=true;
        
		return check;
	}
	public int questions(String options) {
		// TODO Auto-generated method stub
		int marks=0;
		return marks;
		}

	public int deleteQuestions(String topic, int qno) {
		int status=0;
		try {
			Connection connection = Util.getDBConnection();
			String query = "DELETE FROM "+topic+" WHERE qno = "+qno+" ";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
	
			status = preparedStatement.executeUpdate();
			} catch (Exception e) {
			System.out.println(e);
			}
		return status;
	}
	public int EditedQuestions(String topic, int qno, String q, String o1, String o2, String o3, String o4,
			String ans) {
		// TODO Auto-generated method stub
		int status=0;
		try {
			Connection connection = Util.getDBConnection();
			String query = "DELETE FROM "+topic+" WHERE qno = "+qno+" ";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			status = preparedStatement.executeUpdate();
			String query1 ="insert into " +topic+" values("+qno+",'"+q+"','"+o1+"','"+o2+"','"+o3+"','"+o4+"','"+ans+"')";
			PreparedStatement preparedStatement1 = connection.prepareStatement(query1);
			status = preparedStatement1.executeUpdate();
			} catch (Exception e) {
			System.out.println(e);
			}
		return status;
	}
	public int addnewtopic(String topicname) {
		// TODO Auto-generated method stub
		int count1=0;
		try {
			
			Connection con = Util.getDBConnection();
			String createTableQuery="create table "+topicname+" (qno number(4), q varchar(50),o1 varchar(50),o2 varchar(50),o3 varchar(50),	o4 varchar(50),	ans varchar(50),primary key(qno))";
		
			PreparedStatement ps=con.prepareStatement(createTableQuery);
			int count=ps.executeUpdate();
	
				String tablenamequery="insert into topics values('"+topicname+"')";
				PreparedStatement p=con.prepareStatement(tablenamequery);
				count1=p.executeUpdate();
			
			
			
		}
		catch (Exception e) {
			System.out.println(e);
			}
		return count1;
		
	}
	public int addnewQuestions(String topic, int qno, String q, String o1, String o2, String o3, String o4, String ans) {
		// TODO Auto-generated method stub
		int count=0;
try {
			
			Connection con = Util.getDBConnection();
		String addquesquery="insert into " +topic+" values("+qno+",'"+q+"','"+o1+"','"+o2+"','"+o3+"','"+o4+"','"+ans+"')";
		PreparedStatement pt=con.prepareStatement(addquesquery);
		 count=pt.executeUpdate();
}
catch (Exception e) {
	System.out.println(e);
	}return count;
	}
	public int AddIntoHistory(int sid, String date, String topic, String score) {
		// TODO Auto-generated method stub
		int count=0;
		try {
			
			Connection con = Util.getDBConnection();
		String query="insert into history values("+sid+",'"+date+"','"+topic+"','"+score+"')";
		PreparedStatement pt=con.prepareStatement(query);
		count=pt.executeUpdate();
		}
		catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}
	public int changeprofpassword(String idd, String currentPassword, String newpass, String conpass) {
		// TODO Auto-generated method stub
		
		int i=0;
		try {
					
					Connection con = Util.getDBConnection();
					Statement st=con.createStatement();
					ResultSet rs=st.executeQuery("select * from professor_details where pid= '"+idd+"' ");
					
					String pass = null;
					String id = null;
					while(rs.next()){
					id=rs.getString(1);
					pass=rs.getString(4);
					} 
					 if (id==null) {
					       return 200;
					    }
					if(pass.equals(currentPassword)&&id.equals(idd)){
					
					Statement st1=con.createStatement();
					i=st1.executeUpdate("update professor_details set ppass= '"+newpass+"' where pid= "+idd+" " );
					
					}
		}
		catch (Exception e) {
			System.out.println(e);
		}return i;
	}
	public int changestupassword(String idd, String currentPassword, String newpass, String conpass) {
		int i=0;
		try {
					Connection con = Util.getDBConnection();
					Statement st=con.createStatement();
					ResultSet rs=st.executeQuery("select * from student_details where sid= '"+idd+"' ");
					String pass = null;
					String id = null;
					while(rs.next()){
					id=rs.getString(1);
					pass=rs.getString(4);
					} 
					 if (id==null) {
					      return 200;
					}
					if(pass.equals(currentPassword)&&id.equals(idd)){
					Statement st1=con.createStatement();
					i=st1.executeUpdate("update student_details set spass= '"+newpass+"' where sid= "+idd+" " );
					}
		}
		catch (Exception e) {
			System.out.println(e);
		}return i;
	}
	
	
	
}
