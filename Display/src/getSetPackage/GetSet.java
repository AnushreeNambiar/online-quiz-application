package getSetPackage;

public class GetSet {
   
    public GetSet() {
        super();
        // TODO Auto-generated constructor stub
    }
        private int sid;
        private String sname;
        private String semail;
        private String spass;

        private int pid;
        private String pname;
        private String pemail;
        private String ppass;
        private String topic;
        
        
        
        
        public GetSet(int sid,int pid, String sname,String pname, String semail,String pemail,String spass,String ppass) {
        super();
        this.sid = sid;
        this.pid = pid;
        this.sname = sname;
        this.pname = pname;
        this.semail = semail;
        this.pemail = pemail;
        this.spass=spass;
        this.ppass=ppass;
        }

        public int getStudentId() {
        return sid;
        }

        public void setStudentId(int sid) {
        this.sid = sid;
        }

        public int getProfessorId() {
        return pid;
        }

        public void setProfessorId(int pid) {
        this.pid = pid;
        }

        public String getStudentName() {
        return sname;
        }

        public void setStudentName(String sname) {
        this.sname = sname;
        }


        public String getProfessorName() {
        return pname;
        }

        public void setProfessorName(String pname) {
        this.pname = pname;
        }


        public String getStudentEmail() {
        return semail;
        }

        public void setStudentEmail(String semail) {
        this.semail = semail;
        }


        public String getProfessorEmail() {
        return pemail;
        }

        public void setProfessorEmail(String pemail) {
        this.pemail = pemail;
        }


        public String getStudentPassword() {
        return spass;
        }

        public void setStudentPassword(String spass) {
        this.spass = spass;
        }

        public String getProfessorPassword() {
        return ppass;
        }

        public void setProfessorPassword(String ppass) {
        this.ppass = ppass;
        }
       

        public String gettopicname() {
     
		return topic;
        }

        public void settopicname(String topic) {
        this.topic = topic;
        }
       
        }
    

