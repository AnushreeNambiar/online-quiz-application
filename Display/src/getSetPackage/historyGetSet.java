package getSetPackage;

public class historyGetSet {
	public historyGetSet() {
        super();
        // TODO Auto-generated constructor stub
    }
	private int sid;
	private String datetime;
	private String topic;
	private String score;
	
	public historyGetSet(int sid,String datetime,String topic,String score)
	{
		super();
		this.setSid(sid);
		this.setDatetime(datetime);
		this.setTopic(topic);
		this.setScore(score);
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
	
	

}
