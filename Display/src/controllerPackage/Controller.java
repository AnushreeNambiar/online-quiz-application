package controllerPackage;
import java.lang.*;
import java.util.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import getSetPackage.GetSet;
import getSetPackage.historyGetSet;
import getSetPackage.questionGetSet;
import servicesPackage.Services;
import utilPackage.Util;

public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out=resp.getWriter();
		HttpSession session=req.getSession();
		String trigerFrom = req.getParameter("TheButton");
		if (trigerFrom.equals("RegisterProfessor")) {
			int pid = Integer.parseInt(req.getParameter("pid"));
			String pname = req.getParameter("pname");
			String pemail = req.getParameter("pemail");
			String ppass= req.getParameter("ppass");
			GetSet gs=new GetSet();
			gs.setProfessorId(pid);
			gs.setProfessorName(pname);
			gs.setProfessorEmail(pemail);
			gs.setProfessorPassword(ppass);
			Services s = new Services();
			String status = s.insertProfessor(gs);
			if (status.equals("Success")) {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Registration Sucessfull\")");
				out.println("window.location.href = \"regp.jsp\"");
				out.println("</script>");	
			} else {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Registration Unsucessfull\")");
				out.println("window.location.href = \"regp.jsp\"");
				out.println("</script>");
			}
		}
		
		if (trigerFrom.equals("RegisterStudent")) {
			int sid = Integer.parseInt(req.getParameter("sid"));
			String sname = req.getParameter("sname");
			String semail = req.getParameter("semail");
			String spass= req.getParameter("spass");
			GetSet gs=new GetSet();
			gs.setStudentId(sid);
			gs.setStudentName(sname);
			gs.setStudentEmail(semail);
			gs.setStudentPassword(spass);
			Services s = new Services();
			String status = s.insertStudent(gs);

			if (status.equals("Success")) {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Registration Sucessfull\")");
				out.println("window.location.href = \"regs.jsp\"");
				out.println("</script>");
			} else {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Registration Unsucessfull\")");
				out.println("window.location.href = \"regs.jsp\"");
				out.println("</script>");
			}
			}
		
		if (trigerFrom.equals("LoginProfessor")) {
			int pid = Integer.parseInt(req.getParameter("pid"));
			String ppass= req.getParameter("ppass");
			GetSet gs=new GetSet();
			gs.setProfessorId(pid);
			gs.setProfessorPassword(ppass);
			Services s = new Services();
			String status = s.viewProfessor(gs);
			HttpSession pse=req.getSession();
			
			if (status.equals("true")) {
				pse.setAttribute("pid", pid);
			resp.sendRedirect("selecttopic.jsp");
			} else {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Login Unsuccessful\")");
				out.println("window.location.href = \"login1.jsp\"");
				out.println("</script>");
			}
			}
				
			if (trigerFrom.equals("LoginStudent")) {
			int sid = Integer.parseInt(req.getParameter("sid"));
			String spass= req.getParameter("spass");
			
			GetSet gs=new GetSet();
			gs.setStudentId(sid);
			gs.setStudentPassword(spass);
			Services s = new Services();
			String status = s.viewStudent(gs);
			HttpSession sse=req.getSession();
			
			if (status.equals("false")) {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Login Unsucessfull\")");
				out.println("window.location.href = \"login2.jsp\"");
				out.println("</script>");
			} else {
				sse.setAttribute("sid", sid);
				sse.setAttribute("sname", status);
				resp.sendRedirect("studentselect.jsp");
			}
			}
	
			if (trigerFrom.equals("AddTopic")) {
				String topicname= req.getParameter("topicname");
				GetSet gs=new GetSet();
				Services s = new Services();
				boolean bean=false;
				try {
					bean = Services.addtopic(topicname);
				}
				 catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			if(bean==true) {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Topic Added\")");
				out.println("window.location.href = \"newtopic.jsp\"");
				out.println("</script>");
			}
			else {
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Topic not Added\")");
				out.println("window.location.href = \"newtopic.jsp\"");
				out.println("</script>");
			}
			}
			if (trigerFrom.equals("studentselecttopics")) {
				int sid=Integer.parseInt(req.getParameter("sid"));
				String options= req.getParameter("options");
				session.setAttribute("uoption", options);
				session.setAttribute("sid", sid);
				options=options.trim();
			    System.out.println(options);
			    try {
					ArrayList<questionGetSet> qList=new ArrayList<questionGetSet>();
							qList=getAllquestions(options);
					System.out.println(qList);
					req.setAttribute("sid", sid);
					req.setAttribute("qList",qList);
					req.getRequestDispatcher("quizz.jsp").forward(req, resp);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (trigerFrom.equals("professorselecttopics")) {
				String options= req.getParameter("options");
				//options=options.trim();
			 
			    try {
					ArrayList<questionGetSet> qList=new ArrayList<questionGetSet>();
							qList=getAllquestions(options);
					req.setAttribute("qList",qList);
					req.setAttribute("options", options);
					req.getRequestDispatcher("editquestions.jsp").forward(req, resp);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (trigerFrom.equals("AnswerSubmit")) {
				ArrayList<questionGetSet> qList=new ArrayList<questionGetSet>();
				//String options = req.getParameter("");
			}
			if (trigerFrom.equals("QA")) {
				String topic= req.getParameter("topic");
				req.setAttribute("topic",topic);
				req.getRequestDispatcher("add.jsp").forward(req, resp);
				}
			
			if(trigerFrom.equals("addnewquestion"))
			{
				String topic=req.getParameter("topic");
				int qno=Integer.parseInt(req.getParameter("qno"));
				String q=req.getParameter("que");
				String o1=req.getParameter("oa");				
				String o2=req.getParameter("ob");
				String o3=req.getParameter("oc");
				String o4=req.getParameter("od");
				String ans=req.getParameter("an");
				
				Services s=new Services();
				int status=s.addnewQuestions(topic,qno,q,o1,o2,o3,o4,ans);
				if(status==1)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question Added\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
				}
				else
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question not Added\")");
					out.println("window.location.href = \"add.jsp\"");
					out.println("</script>");
				}	
			}
			
			if (trigerFrom.equals("EditQuestion")) {
				String topic= req.getParameter("topic");
				int qno = Integer.parseInt(req.getParameter("qno"));
				String q=" ",o1=" ",o2=" ",o3=" ",o4=" ",ans=" ";
				ArrayList<questionGetSet> qList=new ArrayList<questionGetSet>();
				try {
					qList=getAllquestions(topic);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(questionGetSet questionGetSet1 : qList) {
				 if(questionGetSet1.getQno()==qno)
				 {
					 q=questionGetSet1.getQ();
					 o1=questionGetSet1.getO1();
					 o2=questionGetSet1.getO2();
					 o3=questionGetSet1.getO3();
					 o4=questionGetSet1.getO4();
					 ans=questionGetSet1.getAns();
				 }
				}
				req.setAttribute("topic",topic);
				req.setAttribute("qno",qno);
				req.setAttribute("q",q);
				req.setAttribute("o1",o1);
				req.setAttribute("o2",o2);
				req.setAttribute("o3",o3);
				req.setAttribute("o4",o4);
				req.setAttribute("ans",ans);	
				req.getRequestDispatcher("Edit.jsp").forward(req, resp);
	
			}

			if (trigerFrom.equals("AnswerSubmit")) {
				ArrayList<questionGetSet> qList=new ArrayList<questionGetSet>();
				//String options = req.getParameter("");
			}
			
			if (trigerFrom.equals("changeprofpassword")) {
				String idd=req.getParameter("idd");
				String currentPassword=req.getParameter("current");
				String newpass=req.getParameter("new");
				String conpass=req.getParameter("confirm");
				Services s=new Services();
			
				int status=s.changeprofpassword(idd,currentPassword,newpass,conpass);
				
				if(status==200)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"User not found (Invalid ID) \")");
					out.println("window.location.href = \"login1.jsp\""); 
					out.println("</script>");
				}
				else if(status>0)
				{
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Password Changed Successfully\")");
				out.println("window.location.href = \"login1.jsp\""); 
				out.println("</script>");
				}
				
				else{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Invalid Current Password\")");
					out.println("window.location.href = \"login1.jsp\""); 
					out.println("</script>");
				}			
			}
			
			if (trigerFrom.equals("changestupassword")) {
				String idd=req.getParameter("idd");
				String currentPassword=req.getParameter("current");
				String newpass=req.getParameter("new");
				String conpass=req.getParameter("confirm");
				Services s=new Services();
			
				int status=s.changestupassword(idd,currentPassword,newpass,conpass);
				
				if(status==200)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"User not found (Invalid ID) \")");
					out.println("window.location.href = \"login2.jsp\""); 
					out.println("</script>");
				}
				else if(status>0)
				{
				out.println("<script type=\"text/javascript\">");
				out.println("alert(\"Password Changed Successfully\")");
				out.println("window.location.href = \"login2.jsp\""); 
				out.println("</script>");
				}
				
				else{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Invalid Current Password\")");
					out.println("window.location.href = \"login2.jsp\""); 
					out.println("</script>");
				}			
			}
			
			
			if (trigerFrom.equals("submitQuiz")) {
				
				String options= (String) session.getAttribute("uoption");
				int sid=(Integer) session.getAttribute("sid");
				int c=0;
				ArrayList<String> qList1=new ArrayList<String>();
				try {
					qList1=getAllquestionans(options);
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("user checked answer");
				int size = qList1.size();
				for(int i=0;i<size;i++) {
			   String ans=(String)req.getParameter("q"+i);
				   System.out.println(ans);
				   String v=qList1.get(i);
				   if(ans.equals(v))
					   c++;
				}
				String score=Integer.toString(c);
				//int sid= Integer.parseInt(req.getParameter("sid"));
				String date= req.getParameter("date");
				String topic= options;
				System.out.println(sid+" "+date+" "+topic+" "+" "+score);
				Services s=new Services();
				s.AddIntoHistory(sid, date, topic, score);
				
				
				req.setAttribute("score",c);
				req.setAttribute("options", options);
				
				    
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"You've Completed The Test \")");
					//out.println("window.location.href = \"tq.jsp\"");
					out.println("</script>");
					req.getRequestDispatcher("tq.jsp").forward(req, resp);
				
					
				
			}
			if (trigerFrom.equals("DeleteQuestion")) {
				int qno = Integer.parseInt(req.getParameter("qno"));
				String topic= req.getParameter("topic");
				Services s=new Services();
				int status=s.delQuestions(topic,qno);
				
				if(status==1)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question Deleted\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
				}
				else
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question not Deleted\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
				}
			}
				
			
			if (trigerFrom.equals("EditedQuestion")) {
				String topic= req.getParameter("topic");
				int qno = Integer.parseInt(req.getParameter("qno"));
				String q= req.getParameter("q");
				String o1= req.getParameter("o1");
				String o2= req.getParameter("o2");
				String o3= req.getParameter("o3");
				String o4= req.getParameter("o4");
				String ans= req.getParameter("ans");
				Services s=new Services();
				int status=s.EditedQuestions(topic,qno,q,o1,o2,o3,o4,ans);
				if(status==1)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question Edited\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
					
				}
				else
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Question not Edited\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
				}
		}
			if(trigerFrom.equals("newtopic"))
			{
				int match=0;
				Services s=new Services();
				
				String topicname=req.getParameter("add");
				System.out.print(topicname);
				match=s.addnewtopic(topicname);
				if(match>0)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Topic Added\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
					
				}
				else
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Topic Not Added\")");
					out.println("window.location.href = \"selecttopic.jsp\"");
					out.println("</script>");
				}			
			}
	
			if(trigerFrom.equals("Logout"))
			{
				HttpSession session1=req.getSession();
				//ServletRequest ser = null;
				session1.removeAttribute("pid");
				 session1.invalidate();
					req.getRequestDispatcher("login1.jsp").forward(req, resp);
				
			}
			
			if(trigerFrom.equals("logout"))
			{

				//ServletRequest ser = null;
				HttpSession logoutses=req.getSession();
				logoutses.removeAttribute("sid");
				logoutses.invalidate();
				req.getRequestDispatcher("login2.jsp").forward(req, resp);
				
			}
			if(trigerFrom.equals("AddIntoHistory"))
			{
				int sid = Integer.parseInt(req.getParameter("sid"));
				String date= req.getParameter("date");
				String topic= req.getParameter("topic");
				String score= req.getParameter("score");
				System.out.println(sid+" "+date+" "+topic+" "+score);
				Services s=new Services();
				int status=s.AddIntoHistory(sid,date,topic,score);
				if(status==1)
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Score Added into History\")");
					out.println("window.location.href = \"studentselect.jsp\"");
					out.println("</script>");
				}
				else
				{
					out.println("<script type=\"text/javascript\">");
					out.println("alert(\"Score not added into history\")");
					out.println("window.location.href = \"studentselect.jsp\"");
					out.println("</script>");
				}
			}
			if(trigerFrom.equals("displayHistory"))
			{
				int sid = Integer.parseInt(req.getParameter("sid"));
				//int sid=1;
				String datetime="";
				String topic="";
				String score="";
				ArrayList<historyGetSet> hList=new ArrayList<historyGetSet>();
				try {
					hList=gethistory(sid);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(historyGetSet historyGetSet1 : hList)
				{
					sid=historyGetSet1.getSid();
					datetime=historyGetSet1.getDatetime();
					topic=historyGetSet1.getScore();
					score=historyGetSet1.getScore();
					
				}
				req.setAttribute("sid",sid);
				req.setAttribute("datetime",datetime);
				req.setAttribute("topic",topic);
				req.setAttribute("score",score);
				req.setAttribute("hList", hList);
				
				req.getRequestDispatcher("history.jsp").forward(req, resp);
				
			}
	
	}
	public static ArrayList<historyGetSet> gethistory(int sid) throws SQLException, ClassNotFoundException 
	{			    
			Connection conn = Util.getDBConnection();
	        Statement stm;
	        stm = conn.createStatement();
	        String sql = "Select * From history where sid="+sid+ "order by datetime desc";
	        ResultSet rst;
	        rst = stm.executeQuery(sql);
	        ArrayList<historyGetSet> qList = new ArrayList<historyGetSet>();
	        while (rst.next()) {
	            historyGetSet questions = new historyGetSet(rst.getInt("sid"),rst.getString("datetime"), rst.getString("topic"), rst.getString("score"));
	            qList.add(questions);
	        }
	        return qList;
	    }
			
	

	    	
			public static ArrayList<questionGetSet> getAllquestions(String options) throws SQLException, ClassNotFoundException 
			{			    
					Connection conn = Util.getDBConnection();
			        Statement stm;
			        stm = conn.createStatement();
			        String sql = "Select * From "+options;
			        ResultSet rst;
			        rst = stm.executeQuery(sql);
			        ArrayList<questionGetSet> qList = new ArrayList<questionGetSet>();
			        while (rst.next()) {
			            questionGetSet questions = new questionGetSet(rst.getInt("qno"),rst.getString("q"), rst.getString("o1"), rst.getString("o2"), rst.getString("o3"), rst.getString("o4"), rst.getString("ans") );
			            qList.add(questions);
			        }
			        return qList;
			    }
			public static ArrayList<String> getAllquestionans(String options) throws SQLException, ClassNotFoundException 
			{	 
					Connection conn = Util.getDBConnection();
			        Statement stm;
			        stm = conn.createStatement();
			        String sql = "Select * From "+options;
			        ResultSet rst;
			        rst = stm.executeQuery(sql);
			        ArrayList<questionGetSet> qListans = new ArrayList<questionGetSet>();
			        while (rst.next()) {
			            questionGetSet questions = new questionGetSet(rst.getString("ans"));
			            qListans.add(questions);
			            
			        }
			        ArrayList<String> x=new ArrayList<String>();
			        for(questionGetSet y:qListans)
			        {
			        	x.add(y.getAns() );
			        }
			            Iterator i =x.iterator();
			            System.out.println("The correct answers are:");
			            while (i.hasNext()) {
			               System.out.println(i.next());
			        
			    }
			            return x;
	}
}
	

	