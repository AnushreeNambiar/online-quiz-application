package servicesPackage;



import getSetPackage.GetSet;

import java.sql.SQLException;

import daoPackage.Dao;

public class Services{

	   public String insertProfessor(GetSet gs) {
		   //Validation
		   String status = "";
		   Dao pDao = new Dao();
		   int count = pDao.insertProfessor(gs);
		   if(count == 1)
			   status = "Success";
		   else
			   status = "Fail";
		   return status;
		 }
	   public String insertStudent(GetSet gs) {
		   //Validation
		   String status = "";
		   Dao pDao = new Dao();
		   int count = pDao.insertStudent(gs);
		   if(count == 1)
			   status = "Success";
		   else
			   status = "Fail";
		   return status;
	    }
	   
	   public String viewProfessor(GetSet gs) {
			// TODO Auto-generated method stub
			   Dao pDao = new Dao();
			   String sts = pDao.viewProfessor(gs);
			return sts;
		}

		
		public String viewStudent(GetSet gs) {
			// TODO Auto-generated method stub
		       Dao pDao = new Dao();
			   String sts = pDao.viewStudent(gs);
			return sts;
		}
		public static boolean addtopic(String topicname) throws SQLException {
			// TODO Auto-generated method stub
		     Dao pDao = new Dao();
		     boolean topicbean=pDao.addtopic(topicname);
			return topicbean;
		}
		public int displayquestions(String options) {
			// TODO Auto-generated method stub
			Dao pDao=new Dao();
			int marks=pDao.questions(options);
			return marks;
		}
		public int delQuestions(String topic, int qno) {
			
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int status=d.deleteQuestions(topic,qno);
					return status;
		}
		public int EditedQuestions(String topic, int qno, String q, String o1, String o2, String o3, String o4,
				String ans) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int status=d.EditedQuestions(topic,qno,q,o1,o2,o3,o4,ans);
					return status;
		}
		public int addnewtopic(String topicname) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int sta=d.addnewtopic(topicname);
			return sta;
		}
		public int addnewQuestions(String topic, int qno, String q, String o1, String o2, String o3, String o4, String ans) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int sta=d.addnewQuestions(topic,qno,q,o1,o2,o3,o4,ans);
			return sta;
		}
		public int AddIntoHistory(int sid, String date, String topic, String score) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int sta=d.AddIntoHistory(sid,date,topic,score);
			return sta;
		}
		public int changeprofpassword(String idd, String currentPassword, String newpass, String conpass) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int sta=d.changeprofpassword(idd,currentPassword,newpass,conpass);
			return sta;
		}
		public int changestupassword(String idd, String currentPassword, String newpass, String conpass) {
			// TODO Auto-generated method stub
			Dao d=new Dao();
			int sta=d.changestupassword(idd,currentPassword,newpass,conpass);
			return sta;
		}

}
