<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html lang="en">
<head >
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<title>login1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="logincss2.css">
	<link rel="stylesheet" type="text/css" href="logincss1.css">
</head>

<body style="background-position:fixed">

<script src="login1.js"></script>
  

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="home.jsp" style="text-decoration:bold;color:white">KwizWizz !</a>
    </div>
    <ul class="nav navbar-nav">
	  <li><a href="#">Developer Content</a></li>
      <li><a href="#">About Us</a></li>
      <li><a href="#">Contact</a></li>
  </div>
</nav>
	<div class="limiter">
		<div style="background-image: url('loginbg.jpg');margin-top:3%;position: fixed;top: 0;width: 100%;height: 100%;background-size: cover;">
			<div class="wrap-login100 p-t-190 p-b-30">
				<center><form name="RegForm" onsubmit="return sub()" action="Controller" method="post" style="position:fixed;margin-left:38%">
				<div><h1 style="font-size:50px;color:black"><b>Professor Login</b></h1></div><br>
       
					<div class="wrap-input100 validate-input m-b-10" data-validate = "EmployeeId is required">
						<input class="input100" type="number" name="pid" placeholder="Employee-Id" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
						<input class="input100" type="password" name="ppass" placeholder="Password" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button name="TheButton"class="login100-form-btn" value="LoginProfessor">
							Login
						</button>
					</div>
					<div class="text-center w-full p-t-25 p-b-230">
						<a href="changeprofpassword.jsp" style="font-size:15px;">
							<b><i>Change Password?</i></b>
						</a>
					</div>
					</form></center>
			</div>
		</div>
	</div>

</body>
</html>