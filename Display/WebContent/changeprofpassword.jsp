<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <!--Navigation Bar-->
<nav class="navbar navbar-inverse navbar-fixed-top">
 <div class="container-fluid">
<div class="navbar-header">
 <a class="navbar-brand" href="home.jsp	" style="text-decoration:bold;color:white">KwizWizz !</a>
</div>
<ul class="nav navbar-nav">
 
 <li><a href="#">Developer Content</a></li>
 <li><a href="#">About Us</a></li>
 <li><a href="#">Contact</a></li>

</ul>

 <ul class="nav navbar-nav navbar-right">
    <li><a href="conglogout.jsp"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
</ul>

 </div>
</nav>
  <script type="text/javascript">
function sub()
  {
   var opas=document.forms["RegForm"]["current"];
   var pas=document.forms["RegForm"]["new"];
   var cp=document.forms["RegForm"]["confirm"];
   var iid=document.forms["RegForm"]["idd"];
   var x=pas.value;
   var strongRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;
   if(iid.value=="" ||iid.value==null)
     {  
     window.alert("please enter the Employee id");
     return false;
    }
   if(opas.value=="" ||opas.value==null)
     {  
     window.alert("please enter the password");
     return false;
    }
     
   if(pas.value==""|| pas.value==null)
    {  
    window.alert("please enter the password");
    return false;
   }
   if(x.length<6||pas.value.length>20)
   {
       window.alert("please password of length 6 -20");
     pas.focus();
    return false;
   }
 }
 </script>


<style>

 

form{
 width: 50%;
 height:60%;
  padding:15px;
  padding-bottom:10px;
  background-color:white;
  opacity:1;
  border-radius: 0px 0px 10px 10px; 
  margin-left:25%;
  font-size:25px;
}
.header {
  width: 50%;
  margin: 50px auto 0px;
  color: white;
  background-color:black;
  text-align: center;
  border-bottom: none;
  border-radius: 10px 10px 0px 0px;
  padding:15px;
    margin-left:25%;
    margin-top:5%;
    font-size:25px;
}
.button{
  background-color: #D5044A;;
  color: white;
  padding: 14px 15px;
  cursor: pointer;
  width: 13%;
  height:50%;
  font-size:18px; 
}
body
{
 background-image:url('changepass.jpg');
  background-repeat: no-repeat;
  background-size:cover;
  width: 100%;
     height: 100%;
}
</style>
</head>
<body>

<div style="text-align:center" class="header"><b><i>Change Password</i></b></div>
<form action="Controller" method="post" text-align:center name="RegForm" onsubmit="return sub()">
<b>Enter ID</b><br>
<input type="text"  name="idd" id="pass1" placeholder="Enter Your ID" class="form-control input-lg" style="margin-left:25%;width:50%"><br>
<b>Old Password</b><br>
<input type="password"  name="current" id="pass1" placeholder="Enter Old Password" class="form-control input-lg" style="margin-left:25%;width:50%"><br>
<b>New Password</b><br>
<input type="password"  name="new" id="pass1" placeholder="Enter New Password"  class="form-control input-lg" style="margin-left:25%;width:50%"><br>
<b>Confirm Password<b/><br>
<input type="password"  name="confirm" placeholder="Confirm Password"  class="form-control input-lg" style="margin-left:25%;width:50%"><br>
<button type="submit" name="TheButton" value="changeprofpassword"  class="btn btn-primary btn-lg" style="background-color:#FA7FB5;">Done</button>

</form></div>
</body>
</html>