<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="registerstyle.css">
		<script src="regp.js"></script>
</head>
<!---------------------------------------------- -->
<body>
<div class="wrapper" style="background-image: url('bulb.jpg');">
		<div class="inner">	
				<form name="RegForm"  action="Controller" method="post" >
					<h3>Professor Registration</h3>
					<div class="form-group" >
						<input type="text" placeholder="First Name"  class="form-control" name="pname" required>
						<input type="text" placeholder="Last Name"  class="form-control" name="LastName">
					</div><br>
					<div class="form-wrapper">
						<input type="text" placeholder="Employee Id"  class="form-control" name="pid" required >
						<i class="zmdi zmdi-account"></i>
					</div>
					<div class="form-wrapper">
						<input type="text" placeholder="Email Address" class="form-control" name="pemail" required>
						<i class="zmdi zmdi-email"></i>
					</div>
					<div class="form-wrapper">
						<select name="" id="" class="form-control">
							<option value="" disabled selected>Gender</option>
							<option value="male">Male</option>
							<option value="femal">Female</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Password" class="form-control"  name="ppass" required>
						<i class="zmdi zmdi-lock"></i>
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Confirm Password" class="form-control" name="cp" required >
						<i class="zmdi zmdi-lock"></i>
					</div>
					<button type="submit" onclick="return sub()" value="RegisterProfessor" name="TheButton">Register
						<i class="zmdi zmdi-arrow-right"></i>
					</button>
					<br> <br>
			<div style="font-size:18px;font-family:Cursive">
					<center>Already Registered?
					    <a href="login1.jsp">Login</a> </center></div>
				</form>
			</div>
		</div>
	
	</body>
 </html>
 
