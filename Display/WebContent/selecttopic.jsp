<%@page import="java.sql.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="bootstrap.css" type="text/css" rel="stylesheet">
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<!--Navigation Bar-->
<nav class="navbar navbar-inverse navbar-fixed-top">
 <div class="container-fluid">
<div class="navbar-header">
 <a class="navbar-brand" href="home.jsp	" style="text-decoration:bold;color:white">KwizWizz !</a>
</div>
<ul class="nav navbar-nav">
 
 <li><a href="#">Developer Content</a></li>
 <li><a href="#">About Us</a></li>
 <li><a href="#">Contact</a></li>

</ul>

 <ul class="nav navbar-nav navbar-right">
    <li><a href="conglogout.jsp"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
</ul>

 </div>
</nav>

<style>

form{
 width: 50%;
 height:60%;
  padding:15px;
  padding-bottom:10px;
  background-color:white;
  opacity:0.6;
  border-radius: 0px 0px 10px 10px;
  margin-left:25%;
  font-size:25px;
}
.header {
  width: 50%;
  margin: 50px auto 0px;
  color: white;
  background-color:black;
  text-align: center;
  border-bottom: none;
  border-radius: 10px 10px 0px 0px;
  padding:15px;
    margin-left:25%;
margin-top:5%;
font-size:25px;
}
.button{
  background-color: #D5044A;
  color:black;
  margin-left:10%;
  margin-bottom:7%;
  padding: 14px 15px;
  cursor: pointer;
  width: 40%;
  height:50%;
  font-size:18px;
  text-color:black;
}
body
{
 background-image:url('beach.jpg');
  background-repeat: no-repeat;
  background-size:cover;
  width: 100%;
height: 100%;
}


</style>

</head>

<body style="text-align:center;">
<div style="text-align:center" class="header"><b>SELECT THE TOPIC</b></div>
<form style="height:100%" action="Controller" method="post" >  
  <div> <center><button class="form-control input-lg"  formaction="newtopic.jsp" type="submit" style="margin-left:5%;margin-top:3%;margin-bottom:9%;width:30%;background-color:#D5044A;color:white">ADD NEW TOPIC</button></center> </div>
  <div> <center><h2 style="text-decoration:bold;margin-bottom:9%">OR</h2></center>
    <div style="width:50%;color:#D5044A;font-size:5px;padding-bottom:5%"> 

 
  <select class="form-control input-lg" name="options" style="color:black;margin-left:50%"> 
   <option style="color:black;margin-bottom:50%">Select From Existing Topics</option>
               <% 
    try{
     
     String Query = "SELECT * FROM topics";
     Class.forName( "oracle.jdbc.driver.OracleDriver"); //.newInstance();
     
     
     Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl","hr", "hr");
     
     Statement stm = conn.createStatement();
     
     ResultSet rs = stm.executeQuery(Query);
     
     while(rs.next()){           
      %>
         <option value="<%=rs.getString("topicname")%>"><%=rs.getString("topicname")%></option>
            
      <%
     }
     
    }catch(Exception ex){
     ex.printStackTrace();
     out.println("Error: "+ex.getMessage());
    }
               %>
               
                </select>
                </div>
                
	 <div>
	 	<center><button name="TheButton" value="professorselecttopics" class="form-button input-lg" type="submit" style="margin-left:5%;margin-bottom:7%;width:30%;background-color:#D5044A;color:white">SUBMIT</button> </center>
	 </div>
	</div>
 </form>
 <%
response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);
if(session.getAttribute("pid")==null)
	response.sendRedirect("home.jsp");
	
%>
</body>
</html>