  function sub()
  {
   var name = document.forms["RegForm"]["pname"]; 
   var empno= document.forms["RegForm"]["pid"]; 
    var mail=document.forms["RegForm"]["pemail"]; 
    var pas=document.forms["RegForm"]["ppass"]; 
    var cp=document.forms["RegForm"]["cp"]; 
    var cha = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/; 
	var reg=/^[0-9]*$/;
	var m=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;
 
   if(!name.value.match(cha))
    {  
	window.alert("Please enter the name in characters");
    return false;
   }
        
   if(!mail.value.match(m))
   {
   window.alert("Email invalid");
	  return false;
   }
   if(pas.value.length<6||pas.value.length>20)
   {
   	window.alert("Please enter password of length 6-20");
    return false;
   }
    if(!pas.value.match(paswd))
   {
   window.alert("Password invalid!!! should contain 1 special character, 1 digit, 1 alphabet");
   return false;
   }
  if(cp.value!=pas.value)
   {
   window.alert("You have entered a different password!");
	  return false;
   }
  }
  