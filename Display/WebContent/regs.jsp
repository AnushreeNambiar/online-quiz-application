<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="registerstyle.css">
		<script>
		  function sub()
		  {
		   var name = document.forms["RegForm"]["sname"]; 
		   var empno= document.forms["RegForm"]["sid"]; 
		    var mail=document.forms["RegForm"]["semail"]; 
		    var pas=document.forms["RegForm"]["spass"]; 
		    var cp=document.forms["RegForm"]["cp"]; 
		    var cha = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/; 
			var reg=/^[0-9]*$/;
			var m=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,20}$/;
		 
		   if(!name.value.match(cha))
		    {  
			window.alert("Please enter the name in characters");
		    return false;
		   }
		      
		   
		   if(empno.value==null || empno.value=="" )
		   {
		   window.alert("Please enter the student id");
			  return false;
		   }
		      
		   if(!empno.value.match(reg))
		   {
		   window.alert("Please enter the student id in numbers");
			  return false;
		   }
		        
		   if(!mail.value.match(m))
		   {
		   window.alert("Email invalid");
			  return false;
		   }
		   if(pas.value.length<6||pas.value.length>20)
		   {
		   	window.alert("Please enter password of length 6-20");
		    return false;
		   }
		    if(!pas.value.match(paswd))
		   {
		   window.alert("Password invalid!!! should contain 1 special character, 1 digit, 1 alphabet");
		   return false;
		   }
		  if(cp.value!=pas.value)
		   {
		   window.alert("You have entered a different password!");
			  return false;
		   }
		  }
		  
		</script>
</head>
<!---------------------------------------------- -->
<body>
<div class="wrapper" style="background-image: url('bulb.jpg');">
		<div class="inner">	
				<form name="RegForm"  action="Controller" method="post" >
					<h3>Student Registration</h3>
					<div class="form-group" >
						<input type="text" placeholder="First Name"  class="form-control" name="sname" required>
						<input type="text" placeholder="Last Name"  class="form-control" name="LastName">
					</div><br>
					<div class="form-wrapper">
						<input type="text" placeholder="Student Id"  class="form-control" name="sid" required >
						<i class="zmdi zmdi-account"></i>
					</div>
					<div class="form-wrapper">
						<input type="text" placeholder="Email Address" class="form-control" name="semail" required>
						<i class="zmdi zmdi-email"></i>
					</div>
					<div class="form-wrapper">
						<select name="" id="" class="form-control">
							<option value="" disabled selected>Gender</option>
							<option value="male">Male</option>
							<option value="femal">Female</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Password" class="form-control"  name="spass" required>
						<i class="zmdi zmdi-lock"></i>
					</div>
					<div class="form-wrapper">
						<input type="password" placeholder="Confirm Password" class="form-control" name="cp" required >
						<i class="zmdi zmdi-lock"></i>
					</div>
					<button type="submit" onclick="return sub()" value="RegisterStudent" name="TheButton">Register
						<i class="zmdi zmdi-arrow-right"></i>
					</button>
					<br> <br>
			<div style="font-size:18px;font-family:Cursive">
					<center>Already Registered?
					    <a href="login2.jsp">Login</a> </center></div>
				</form>
			</div>
		</div>
	
	</body>
 </html>
 
