<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.sql.*"%>
    <%@page import="java.util.*" %>
    <%@page import="getSetPackage.questionGetSet" %>
<!DOCTYPE html>
<html>
<head>
		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
		<!-- STYLE CSS -->
		<link rel="stylesheet" href="registerstyle.css">
		<link rel="stylesheet" href="button.css">
		<script type="text/javascript" src="buttons.js"></script>
		</head>
		
<body style="background-image: url('bulb1.jpg');">
<div class="wrapper" style="background-image: url('bulb.jpg');">
		<div class="inner">	
				<form name="form1"  action="Controller" method="post" >
					<h3>Questions:</h3>
					<div class="form-wrapper">
					
					<div class="swappy-radios" role="radiogroup" aria-labelledby="swappy-radios-label">
					<%questionGetSet questionGetSet=new questionGetSet();
					ArrayList<questionGetSet> qList=(ArrayList<questionGetSet>)request.getAttribute("qList");
					for(questionGetSet questionGetSet1 : qList) {
						%> <h3> <% out.println(questionGetSet1.getQ());%> </h3>
						<br>
						<label>
					    <input type="radio" name="<%=questionGetSet1.getQno() %>" value =<%=questionGetSet1.getO1() %> />
					    <span class="radio"></span>
					    <span><% out.println(questionGetSet1.getO1()); %></span>
					  </label>
    					<br>
						<label>
					    <input type="radio" name="<%=questionGetSet1.getQno() %>" value =<%=questionGetSet1.getO2() %> />
					    <span class="radio"></span>
					    <span><% out.println(questionGetSet1.getO2()); %></span>
					  </label>
    					<br>
						<label>
					    <input type="radio" name="<%=questionGetSet1.getQno() %>" value =<%=questionGetSet1.getO3() %>/>
					    <span class="radio"></span>
					    <span><% out.println(questionGetSet1.getO3()); %></span>
					  </label>
    					<br>
						<label>
					    <input type="radio" name="<%=questionGetSet1.getQno() %>" value =<%=questionGetSet1.getO4() %>/>
					    <span class="radio"></span>
					    <span><% out.println(questionGetSet1.getO4()); %></span>
					  </label>
					  <label>
					    <input type="radio" name="<%=questionGetSet1.getQno() %>" value ="none of the above" checked/>
					    <span class="radio"></span>
					    <span><% out.println("None of the above"); %></span>
					  </label>
					  
    					<br>
    					<hr><%
					}
					%>
					</div>
					</div>
					<br>
					<button type="submit" >Get your grades
						<i class="zmdi zmdi-arrow-right"></i>
					</button>
					<br> <br>
				</form>
				<p>Your grade is: <span id="grade">__</span></p>
<p id="grade2"></p>
			</div>
		</div> 
	<script>
	var x;
	var result=0;
       document.getElementById("form1").onsubmit=function() {
    	   questionGetSet questionGetSet=new questionGetSet();
    	   ArrayList<questionGetSet> qList=(ArrayList<questionGetSet>)request.getAttribute("qList");
    	   for(questionGetSet questionGetSet1 : qList){
    		   n=questionGetSet1.getQno();
    		   x=parseInt(document.querySelector('input[name=n]:checked').value);
    		   result=result+x;
    			   }       
	   document.getElementById("grade").innerHTML = result;
	   return false;
	   }
       </script>
	</body>
 </html>