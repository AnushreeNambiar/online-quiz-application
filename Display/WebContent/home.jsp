<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
    <title>Homepage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body style="background-color:#21B7CC" >

 

<!--Navigation Bar-->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" style="text-decoration:bold;color:white">KwizWizz !</a>
    </div>
    <ul class="nav navbar-nav">
   
      <li><a href="#">Developer Content</a></li>
      <li><a href="#">About Us</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="ps2.jsp"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="ps1.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>

 

 

<!--Carousel-->
<div class="container" style="padding:50px;height:10%;width:60%;align:center;margin:100px 270px 0px 270px;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

 

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

 

      <div class="item active">
          <a href="https://worldstrides.com/blog/2016/07/10-interesting-facts-paris/">
            <img src="paris.jpg" href="https://worldstrides.com/blog/2016/07/10-interesting-facts-paris/" alt="paris" style="width:100%;">
            <div class="carousel-caption">
              <h3>Paris</h3>
              <p>Visitors to the Eiffel Tower have to climb 1,665 steps to reach the top – unless they take the elevator!</p>
            </div>
          </a>
      </div>

 

      <div class="item">
          <a href="https://worldstrides.com/blog/2016/12/10-facts-venice/" >
            <img src="venice.jpg" alt="venice" style="width:100%;">
            <div class="carousel-caption">
              <h3>Venice</h3>
              <p>Venice is known for its bridges. There are 417 bridges in Venice, and 72 of those are private.!</p>
            </div>
        </a>
      </div>
    
      <div class="item">
        <a href="https://cleantechnica.com/2019/01/06/15-fascinating-or-at-least-interesting-facts-about-elon-musk/">
        <img src="carousel5.jpg"  alt="Elon Musk" style="width:100%;">
        <div class="carousel-caption">
          <h3>Elon Musk</h3>
          <p> Musk dropped out of Stanford after just two days.</p>
        </div>
        </a>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<!-- Who uses quizbuzz-->
<div class="container text-center">    
  <h3><b>Who Uses KwizWhiz ! </b></h3><br>
  <div class="row">
    <div class="col-sm-4">
      <img src="professor.jpg" class="img-responsive" style="width:50%;margin-left:200px;" alt="Image">
      <h3 style="margin-left:225px;">Professors</h3>
    </div>
    <div class="col-sm-4"> 
        <!-- Middle Empty tag-->
    </div>
    <div class="col-sm-4">
         <img src="student.jpg" class="img-responsive" style="width:50%;margin-left:-10px;" alt="Image">
         <h3 style="margin-left:-200px;">Students</h3>
    </div>
  </div>
</div>

 

<!-- Create Online Quiz-->
<div class="container text-center">    
  <h3><b>Create Online Quiz</b></h3><br>
  <div class="row">
      <div class="col-sm-4">
      </div>
     
     <div class="col-sm-4">
      <p>Always wanted to make a quiz, but couldn't find an easy quiz creator to help you out? With our online quiz tool it’s easy to make a quiz in less than five minutes.
      Just follow these simple steps to create online quizzes with our online quiz software.</p>
    </div>
    
    <div class="col-sm-4">
    </div>
    </div>
</div>

 

<!-- Why use KwizWhiz-->
<div class="container text-center">    
  <h3><b>Why Use KwizWhiz !</b></h3><br>
  <div class="row">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4">
      <p>Do you want to engage your target audience in a fun and playful way? 
      or Do you want to appear in a fun quiz of a topic of your choice ? Sounds fun right?
      Well this is the platform where you can do it all ! Go on , give it a try !!
       Take a look at all the features.</p>
    </div>
    <div class="col-sm-4">
    </div>
    </div>
</div>
<!-- About us-->
<div class="container text-center">    
 <hr>
  <div class="row">
            <div class="col-sm-4">
            <b><a class="terms" href="tc.html">Terms & Conditions</a></b>
              
            </div>
            <div class="col-sm-4">
            <b><a class="terms" href="">About Us</a>
             
            </div>
            
            <div class="col-sm-4">
            <b><a class="terms" href="">Contact</a><b><br>
              
                sanjib.saikia@wipro.com<br>
                +91 1234567890</li><br><br>
                Phone support:<br>
                9:30 - 18:00 IST<br>
                Chat support:<br>
                8:30 - 23:00 IST
              
            </div>
            
    </div>
</div>

 

<!-- Footer-->
<div class="container text-center" >    
  <h3><b>Copyright</b></h3><br>
  <div class="row">
    <div class="col-sm-12">
        <p>    &#169; 2019 ARCHITECTS. Infopark,Kakkanad,Kerala, India.  </p>
      
    </div>
    
    </div>
</div>
</body>
</html>